use `gta311-leba2516-sb`;


-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: gta-ins06.fadm.usherbrooke.ca    Database: dw-amcc
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `src_amcc_jobs`
--

DROP TABLE IF EXISTS `src_amcc_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `src_amcc_jobs` (
  `title` varchar(100) DEFAULT NULL,
  `level` varchar(50) DEFAULT NULL,
  `rate` decimal(16,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `src_amcc_jobs`
--

LOCK TABLES `src_amcc_jobs` WRITE;
/*!40000 ALTER TABLE `src_amcc_jobs` DISABLE KEYS */;
INSERT INTO `src_amcc_jobs` VALUES ('Account Manager','Junior',88.0000),('Business Analyst, IT','Experienced',102.0000),('Customer Success Manager','Experienced',106.0000),('Cyber Security Analyst','Experienced',108.0000),('Data Analyst','Experienced',98.0000),('Data Scientist','Experienced',148.0000),('Human Resources (HR) Generalist','General',84.0000),('Human Resources (HR) Manager','General',106.0000),('Information Technology (IT) Consultant','Junior',110.0000),('Information Technology (IT) Director','Senior',196.0000),('Information Technology (IT) Manager','Experienced',128.0000),('Information Technology Specialist','Junior',82.0000),('Marketing Manager','General',106.0000),('Network Engineer','Junior',98.0000),('Program Manager, IT','Senior',162.0000),('Project Manager, (Unspecified Type / General)','Experienced',118.0000),('Project Manager, Information Technology (IT)','Experienced',124.0000),('Project Manager, IT','Senior',158.0000),('Software Engineer','Senior',168.0000),('Systems Engineer','Senior',140.0000),('Software Developer','Experienced',106.0000),('Software Engineer','Senior',128.0000),('Solutions Architect','Senior',168.0000),('Sr. Software Engineer / Developer / Programmer','Senior',146.0000),('Support Technician, Information Technology (IT)','Experienced',62.0000),('Systems Administrator','Experienced',90.0000),('Systems Engineer (Computer Networking / IT)','Junior',98.0000),('Systems Engineer, IT','Junior',98.0000);
/*!40000 ALTER TABLE `src_amcc_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'dw-amcc'
--

--
-- Dumping routines for database 'dw-amcc'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-20  8:29:25