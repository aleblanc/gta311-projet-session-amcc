use `gta311-leba2516-sb`;


-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: gta-ins06.fadm.usherbrooke.ca    Database: dw-amcc
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `src_amcc_employees`
--

DROP TABLE IF EXISTS `src_amcc_employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `src_amcc_employees` (
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `BirthDate` datetime NOT NULL,
  `HireDate` datetime NOT NULL,
  `Address` varchar(100) NOT NULL,
  `City` varchar(25) NOT NULL,
  `Region` varchar(25) NOT NULL,
  `PostalCode` varchar(10) NOT NULL,
  `Country` varchar(25) NOT NULL,
  `HomePhone` varchar(24) NOT NULL,
  `CellPhone` varchar(24) NOT NULL,
  `ReportsTo` int DEFAULT NULL,
  `Salary` decimal(16,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `src_amcc_employees`
--

LOCK TABLES `src_amcc_employees` WRITE;
/*!40000 ALTER TABLE `src_amcc_employees` DISABLE KEYS */;
INSERT INTO `src_amcc_employees` VALUES ('Manuel','Gisele','1984-07-19 00:00:00','2009-03-10 00:00:00','1455 Rue Sherbrooke O','Montréal','QC','H3G 1L2','Canada','(514) 277-5900','(514) 334-4290',0,0.0000),('Hivon','Allie','1984-07-19 00:00:00','2009-03-10 00:00:00','6565 Ch Kildare','Côte-St-Luc','QC','H4W 1B6','Canada','(514) 383-0240','(514) 336-6342',0,0.0000),('Guan','Paul-emile','1984-07-19 00:00:00','2009-03-10 00:00:00','1525 Décarie','Saint-Laurent','QC','H4L 3N4','Canada','(514) 489-5764','(514) 223-5412',0,0.0000),('Chagnon','Dassine','1984-07-19 00:00:00','2009-03-10 00:00:00','91 Av 3E','Verdun','QC','H4G 2X1','Canada','(514) 938-9236','(514) 354-7420',0,0.0000),('Mourad','Elissa','1984-07-19 00:00:00','2009-03-10 00:00:00','8765 Dube','Lasalle','QC','H8R 2S6','Canada','(514) 487-9561','(514) 277-5900',0,0.0000),('Sweeney','Adelya','1984-07-19 00:00:00','2009-03-10 00:00:00','5740 de Salaberry','Montreal','QC','H4J 1J7','Canada','(514) 489-1986','(514) 331-5632',0,0.0000),('Kidd','Corentin','1984-07-19 00:00:00','2009-03-10 00:00:00','1490 Rue Aylwin','Montreal','QC','H1W 3B7','Canada','(514) 725-9966','(438) 382-3260',0,0.0000),('Skinner','Nohan','1984-07-19 00:00:00','2009-03-10 00:00:00','95 Rue Jean-Talon E','Montréal','QC','H2R 1S6','Canada','(450) 671-7465','(514) 747-6849',0,0.0000),('Loubert','Alexa','1984-07-19 00:00:00','2009-03-10 00:00:00','3555 Edouard-Montpetit','Montréal','QC','H3T 1K7','Canada','(514) 728-8318','(514) 272-9304',0,0.0000),('Marcheterre','Quincy','1984-07-19 00:00:00','2009-03-10 00:00:00','1500 Crevier','Saint-Laurent','QC','H4L 2X3','Canada','(450) 926-8035','(438) 384-1034',0,0.0000),('Cahill','Yanky','1984-07-19 00:00:00','2009-03-10 00:00:00','8174 Rue Dora','Lasalle','QC','H8N 1Z7','Canada','(514) 626-5403','(514) 489-5764',0,0.0000),('Caucci','Aleena','1984-07-19 00:00:00','2009-03-10 00:00:00','10403 Av Vianney','Montréal','QC','H2B 2X7','Canada','(514) 277-4255','(514) 277-4255',0,0.0000),('Archambault','Louis-philip','1984-07-19 00:00:00','2009-03-10 00:00:00','7947 Rue Turley','Lasalle','QC','H8N 2A3','Canada','(438) 386-7794','(514) 365-7421',0,0.0000),('Wells','Emy-rose','1984-07-19 00:00:00','2009-03-10 00:00:00','4565 Rue Michel Bibaud','Montreal','QC','H3W 2E1','Canada','(514) 767-5030','(514) 376-6238',0,0.0000),('Tessier','Jenna','1984-07-19 00:00:00','2009-03-10 00:00:00','7947 Rue Turley','Lasalle','QC','H8N 2A3','Canada','(438) 381-6325','(514) 748-8552',0,0.0000),('Monheit','Zach','1984-07-19 00:00:00','2009-03-10 00:00:00','3090 Rue du Quesne','Montréal','QC','H1N 2X2','Canada','(514) 748-6699','(514) 274-9832',0,0.0000),('Snyder','Dimitrios','1984-07-19 00:00:00','2009-03-10 00:00:00','6565 Rue des Écores','Montréal','QC','H2G 2J8','Canada','(514) 595-9572','(514) 487-9561',0,0.0000),('Coles','Malaika','1984-07-19 00:00:00','2009-03-10 00:00:00','6970 Av 26E','Montréal','QC','H1T 3P7','Canada','(514) 484-3613','(514) 277-4255',0,0.0000),('Trad','Zakariya','1984-07-19 00:00:00','2009-03-10 00:00:00','234 Rue Bégin','Longueuil','QC','J4L 3E7','Canada','(514) 642-6526','(514) 489-8393',0,0.0000),('Izzo','Denver','1984-07-19 00:00:00','2009-03-10 00:00:00','1096 Vanier','Laval','QC','H7C 2R7','Canada','(438) 381-3913','(514) 354-7420',0,0.0000),('Boiteau','Souleymane','1984-07-19 00:00:00','2009-03-10 00:00:00','5062 4 Av','Montreal','QC','H1Y 2V1','Canada','(514) 354-7420','(514) 736-0936',0,0.0000),('Atsynia','Patrick','1984-07-19 00:00:00','2009-03-10 00:00:00','8009 Rue Cartier','Montréal','QC','H2E 2K1','Canada','(514) 329-4499','(514) 642-1018',0,0.0000),('Raynault','Andreas','1984-07-19 00:00:00','2009-03-10 00:00:00','6931 Rue Sherbrooke O','Montréal','QC','H4B 1P8','Canada','(438) 386-7794','(514) 364-6375',0,0.0000),('Wiseman','Nelson','1984-07-19 00:00:00','2009-03-10 00:00:00','4615 Av de Hampton','Montréal','QC','H4A 2L5','Canada','(514) 481-9697','(514) 890-1430',0,0.0000),('Morris','Lily-rose','1984-07-19 00:00:00','2009-03-10 00:00:00','4615 Av de Hampton','Montréal','QC','H4A 2L5','Canada','(514) 274-2376','(514) 331-0383',0,0.0000),('Voisard','Nathaniel','1984-07-19 00:00:00','2009-03-10 00:00:00','9322 Airlie','Montreal','QC','H8R 2A7','Canada','(514) 508-8726','(514) 337-4431',0,0.0000),('Galdamez','Allison','1984-07-19 00:00:00','2009-03-10 00:00:00','1673 Coughtry Street St','Laurent','QC','H4L 3H2','Canada','(514) 366-4346','(514) 938-9236',0,0.0000),('Cerone','Lana','1984-07-19 00:00:00','2009-03-10 00:00:00','4860 De Maisonneuve Bl O','Westmount','QC','H3Z 3G2','Canada','(514) 934-9976','(514) 931-5919',0,0.0000),('Hade','Prisha','1984-07-19 00:00:00','2009-03-10 00:00:00','6693 Côte Luc St','Côte-St-Luc','QC','H4V 1G9','Canada','(514) 274-9832','(514) 932-8690',0,0.0000),('Colangelo','Iris','1984-07-19 00:00:00','2009-03-10 00:00:00','983 Desmarchais Av','Verdun','QC','H4H 1S9','Canada','(450) 699-3638','(450) 420-7536',0,0.0000),('Jackson','Wyatt','1984-07-19 00:00:00','2009-03-10 00:00:00','321 Bergevin Street','Lasalle','QC','H8R 3M4','Canada','(514) 737-0427','(514) 744-5634',0,0.0000),('Walter','Meshilem','1984-07-19 00:00:00','2009-03-10 00:00:00','5039 Rue Pérron','Pierrefonds','QC','H8Z 2J2','Canada','(514) 768-9699','(514) 737-0427',0,0.0000);
/*!40000 ALTER TABLE `src_amcc_employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'dw-amcc'
--

--
-- Dumping routines for database 'dw-amcc'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-20  8:55:44