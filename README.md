# Projet de session GTA311 - Rapport final
Gestion des capacités organisationnelles par les données chez AMCC
Produit par Alexis Leblanc-Isabelle (LEBA2516)
### Lire le rapport final sur Gitlab
https://gitlab.com/aleblanc/gta311-projet-session-amcc

## Objectifs
L’objectif principal du projet de gestion des capacités chez AMCC est de valoriser les données pour soutenir la prise de décisions par les gestionnaires de l’entreprise. Le projet présente les sous-objectifs suivants :
* Mettre en place un bac à sable pour l’analyse des données;
    * Créer la structure de données;
    * Peupler la structure avec les données fournies;
* Valider les données fournies;
    * Tester la qualité des données au moyen de requêtes SQL;
    * Améliorer la qualité des données au besoin;
* Démontrer la valeur du bac à sable en réalisant une série d’analyses
    * Répondre à une série de questions d’affaires au moyen de requêtes SQL;
    * Démontrer l’utilité du bac à sable en le connectant aux outils de bureautique usuels.

Le projet exclut la mise en production de la base de données du bac à sable se limitant à une preuve de concept auprès de AMCC.
## Biens livrables
| Date      | Livrable |
| ----------- | ----------- |
| 2021-07-19      | Structure du bac à sable + chargement des données|
| 2021-07-26   | Données corrigées + test des données et données de référence        |
| 2021-08-02   | Analyses et connexions + Requis d'affaires        |
| 2021-08-09   | Rapport final de projet        |
## Modèle de données
![gta311-leba2516-sb.png](https://www.dropbox.com/s/5lftkj09p0bs5g2/gta311-leba2516-sb.png?dl=0&raw=1)

```sql
# Exemple d'un bout de code SQL permettant de créer un objet.
create table d_dates
(
    DateID    int auto_increment not null,
    Date      date               not null,
    Year      int                not null,
    Month     int                not null,
    Day       int                not null,
    IsWeekend tinyint            not null,
    constraint d_dates_pk
        primary key (DateID)
);
```
### Connexion à mon modèle spécifique.
Host : gta-ins06.fadm.usherbrooke.ca
Port : 3306
Username : leba2516
Mot de passe : voir administrateur réseau
Base de données : gta311-leba2516-sb

### Modification par rapport au modèle de données original
- Plusieurs champs varchar() ont du être allongé en raison de la longueur des données présentes dans les tableaux sources.
- Le champ Phone de la table d_client était noté comme non null, mais le tableau source contient beaucoup de données non null. Le champ dans notre base de données accepte donc les champs null.
- Il y a présence de champ fk_xxxx_temp. Ces clés sont des champs temporaires utilisés pour recréer un système de ID fonctionnel à travers la base de données. Il est possible de les supprimer tel qu'indiqué plus tard dans la documentation. J'ai décidé de laisser ces champs afficher dans la représentation de mon modèle de données puisqu'ils jouent un rôle important dans la création de la BD.
- J'ai renommé tous les noms de champ afin de débuter par une majuscule.

### Données sources
Afin de pouvoir utiliser cette documentation, les données doivent être fournis selon le format exact des tables src présenter dans le modèle de données ci-haut. Le nom des tables et le nom des champs doivent être identiques. 
Afin d'améliorer les données en amont, il serait pertinent de fournir des données respectant les consignes suivantes :
- S'assurer qu'il n'y a pas de doublon dans les clients, les compagnies et les employés.
- S'assurer que les champs de nom, prénom et nom de compagnie ne contient pas de caractères spéciaux. Particulièrement pas de "\".
- S'assurer que les données courriel et téléphone des clients sont complets et ne contiennent pas de caractère fautif pour une adresse courriel. (accent, apostrophe, $, %, ?).
- S'assurer que la date de naissance des employés n'est pas identique pour tout les employés.

## Initialisation du projet
Ce projet utilise MySQL.
Collation : utf8mb4_0900_ai_ci
Vous devrez avoir tous les droits afin de pouvoir initialiser le projet sur votre base de données. Les accès pour vous connecter à un serveur pouvant héberger une base de données ne sont pas couverts dans ce projet.

Dans une base de données vierge, rouler les scripts suivants afin d'initialiser le projet

À noter que certains champs ont vu leur nombre de caractères maximal augmenter afin de pouvoir accepter les données provenant des tables sources. Ces changements sont automatiquement pris en compte dans les scripts d'initialisations. Il s'agit des seules différences avec le modèle de données proposé en début de projet.

> À la première ligne de chaque script, la BD à utilisé est déclaré. Veuillez à modifier cette ligne en fonction du nom de votre BD.

**Création de la BD**
```sh
/init/init_db.sql
```
**Création des tables**
```sh
/init/init_tables.sql
```
**Créer les relations entre les tables**
```sh
/init/init_tables_relations.sql
```
**Création des tables sources de données provenant du client**
```sh
/src/amcc-src-clients.sql
/src/amcc-src-companies.sql
/src/amcc-src-contracts.sql
/src/amcc-src-dates.sql
/src/amcc-src-employees.sql
/src/amcc-src-jobs.sql
/src/amcc-src-workhours.sql
```
## Validation et chargement des données

Les scripts suivants permettent de corriger les données présentes dans les tables sources et ensuite de les insérer dans les tables créées précédemment.
Chaque script est décomposé en 4 blocs.
1. **Vérification de la qualité des données :** ces requêtes permettent d'identifier des problèmes de qualités de données qui pourrait être présente dans la table source. Si aucun problème n'est présent, cette requête ne retournera aucun résultat.
2. **Mise à jour des données :** ces requêtes permettent de corriger les problèmes précédemment trouvés. Il est important d'exécuter toutes les requêtes présentes dans cette section afin de corriger les données à importer. Si de nouvelles erreurs sont détectées dans la section 1, il est important d'ajouter des requêtes ici afin d'assurer une correction des données consistante.
3. **Insertion des données :** Cette requête unique, une fois exécutée, va insérer les données de la table source vers notre nouvelle table associée.
4. **Vérification de l'insertion des données :** Il s'agit de requête test permettant d'assurer la qualité des données insérées. Chacune de ces requêtes retournera un résultat vide, sans quoi il y a eu un problème lors de l'insertion. Si certaines conditions spéciales doivent être testées, les ajouter à cette section.

**Le détail des tests se trouve à l'intérieur de chaque script sous forme de commentaire**
```sh
/script/script_insertion_donnees_clients.sql
/script/script_insertion_donnees_companies.sql
/script/script_insertion_donnees_contracts.sql
/script/script_insertion_donnees_dates.sql
/script/script_insertion_donnees_employees.sql
/script/script_insertion_donnees_roles.sql
/script/script_insertion_donnees_workhours.sql
```
### Exemple de problème de qualité de données avec leur correction.
**Vérification visuelle**


![QA_backslash.png](https://www.dropbox.com/s/9txx5xorn8pem7p/QA_backslash.png?dl=0&raw=1)


**FIX**


![FIX_backslash.png](https://www.dropbox.com/s/bnl2jpyhawtplqy/FIX_backslash.png?dl=0&raw=1)

**Vérification des codes postaux**


![QA_code-postal.png](https://www.dropbox.com/s/bm9q64f7gv24e67/QA_code-postal.png?dl=0&raw=1)


**FIX**


![FIX_code-postal.png](https://www.dropbox.com/s/in0wdzyt18lzrk9/FIX_code-postal.png?dl=0&raw=1)

**Vérification des champs courriel**


![QA_courriel.png](https://www.dropbox.com/s/rb9si6acq9jaj7t/QA_courriel.png?dl=0&raw=1)


**FIX**


![FIX_accent.png](https://www.dropbox.com/s/k5f0q8f17w9z6py/FIX_accent.png?dl=0&raw=1)

**Vérification des doublons**


![QA_doublons.png](https://www.dropbox.com/s/ry04rvyvxdmzk1w/QA_doublons.png?dl=0&raw=1)

**FIX**


![FIX_doublons.png](https://www.dropbox.com/s/9x30yo6hbvr2dx1/FIX_doublons.png?dl=0&raw=1)

**Vérification de l'uniformité des données**


![QA_uniformite_valeur.png](https://www.dropbox.com/s/1fojslwwgwqop0x/QA_uniformite_valeur.png?dl=0&raw=1)


**FIX**


![FIX_uniformite_valeur.png](https://www.dropbox.com/s/dq6voykk565fe0c/FIX_uniformite_valeur.png?dl=0&raw=1)


## OPTIONNEL : suppression des tables SRC
Si vous le désirez, vous pouvez exécuter le script suivant afin de supprimer les tables sources.
```sh
/script/script_suppression_tables_src.sql
```

## Manipulation des données
Une fois les données insérées dans la nouvelle BD, il faut mettre à jour certaines informations.
Premièrement, les ID provenant des bases de données sources n'étaient pas tous utilisables facilement. Nous devons donc recréer les liens en fonction de combinaisons de plusieurs champs afin d'assurer l'unicité des entrées et ainsi pouvoir utiliser correctement les ID comme clés primaires et secondaires. À l'aide de colonne temporaire nommée fk_nomDuChamp_temp, nous sommes en mesure de créer une clé secondaire temporaire dans la table f_workhours. Ensuite, il nous est possible d'utiliser cette clée temporaire afin d'assoucier les nouveaux ID des tables associées créer lors de l'importation des données. De cette façon, le lien ne repose plus sur des champs pouvant contenir des erreurs ou bien pouvant être modifié. Par exemple, si nous devions changer le nom d'une compagnie, alors l'utilisation de son nom comme clée secondaire serait problématique alors qu'avec un système de ID indépendant, nous avons la latitude de modifier les champs aux besoins.

Exécuter le script suivant :

```sh
/script/script_manipulation_donnees.sql
```

## OPTIONNEL : suppression des colonnes de clée secondaire
Si vous le désirez, vous pouvez faire la suppression des colonnes de clés secondaires fk_nomDuChamp_temp de la table f_workhours :
```sh
/script/script_suppression_colonne_temp.sql
```

## Valorisation des données
Afin d'accéder aux réponses des questions d'affaires, vous pouvez utiliser les scripts suivants.
```sh
/prj-qry/Prj-qry01.sql
/prj-qry/Prj-qry02.sql
/prj-qry/Prj-qry03.sql
/prj-qry/Prj-qry04.sql
/prj-qry/Prj-qry05.sql
/prj-qry/Prj-qry06.sql
/prj-qry/Prj-qry07.sql
/prj-qry/Prj-qry08-bonus.sql
/prj-qry/Prj-qry09-bonus.sql
```
**En Image**


![qry01.png](https://www.dropbox.com/s/tirb0z2abrbprxj/qry01.png?dl=0&raw=1)
![qry02.png](https://www.dropbox.com/s/gg7vfavdd1tsaow/qry02.png?dl=0&raw=1)
![qry03.png](https://www.dropbox.com/s/wpdv04ybkg8s7hl/qry03.png?dl=0&raw=1)
![qry04.png](https://www.dropbox.com/s/ndigo71ea8llb58/qry04.png?dl=0&raw=1)
![qry05.png](https://www.dropbox.com/s/sof406i7jcs8cce/qry05.png?dl=0&raw=1)
![qry06.png](https://www.dropbox.com/s/3iiuwr1qvtldw32/qry06.png?dl=0&raw=1)
![qry07.png](https://www.dropbox.com/s/8d2c41j7v18yqrz/qry07.png?dl=0&raw=1)
![qry08.png](https://www.dropbox.com/s/yjuqlff2mc5iyst/qry08.png?dl=0&raw=1)

## Connexion aux données
Nous avons connecté la base de données avec l'application Tableau Desktop afin de tester le concept de service autonome (self-service). Vous pouvez voir un aperçue sur les images ci-bas et consulter le fichier à l'emplacement suivant :
```sh
/documents/liste-employes.twb
```
![tableau_liste_1.png](https://www.dropbox.com/s/4paxv3cj1loom1g/tableau_liste_1.png?dl=0&raw=1)
![tableau_liste_2.png](https://www.dropbox.com/s/8ztknismjwrkgd5/tableau_liste_2.png?dl=0&raw=1)

