use `gta311-leba2516-sb`;


/* ----------------------------------------------
*
* Validation et ajout des données de la table src_amcc_clients
*
    fk_contract int            not null,
    fk_client   int            not null,
    fk_date     int            not null,
    fk_employee int            not null,
    fk_role     int            not null,
    Hours       decimal(16, 2) not null,
    Rate        decimal(16, 4) not null,
    Status      varchar(45)    not null
*
* ---------------------------------------------- */

/* ----------------------------------------------
*
* 1. Vérification de la qualitée des données
*
* ---------------------------------------------- */
# a. Doublons
select concat(contractNumber, ' ', employee_firstname, ' ', employee_lastname, ' ', Client_CompanyName,
              ' ', Client_CompanyUnit, ' ', workhours_date) as 'name',
       sum(workhours_hours),
       count(1)
from src_amcc_workhours
group by concat(contractNumber, ' ', employee_firstname, ' ', employee_lastname, ' ',
                Client_CompanyName, ' ', Client_CompanyUnit, ' ', workhours_date)
having count(1) > 1;

# b. Données manquantes
select concat(employee_firstname, ' ', employee_lastname)
from src_amcc_workhours
where src_amcc_workhours.contractNumber is null
   or Client_CompanyName is null
   or Client_CompanyUnit is null
   or src_amcc_workhours.workhours_date is null
   or src_amcc_workhours.employee_lastname is null
   or src_amcc_workhours.employee_firstname is null
   or src_amcc_workhours.employee_roleName is null
   or src_amcc_workhours.workhours_hours is null
   or src_amcc_workhours.workhours_rate is null
   or src_amcc_workhours.workhours_status is null;

# e. Vérification visuelle de présence de "Backslash \" dans les champs texte
select contractNumber,
       Client_CompanyName,
       Client_CompanyUnit,
       employee_lastname,
       employee_firstname,
       employee_roleName
from src_amcc_workhours
where contractNumber like '%\\\\%'
   or Client_CompanyName like '%\\\\%'
   or Client_CompanyUnit like '%\\\\%'
   or employee_lastname like '%\\\\%'
   or employee_firstname like '%\\\\%'
   or employee_roleName like '%\\\\%';


/* ----------------------------------------------
*
* 2. Mise à jour des données
*
* ---------------------------------------------- */

/* ----------------------------------------------
*
* Insertion des données
*
* ---------------------------------------------- */

# Insertion des entrées de temps excluant les doublons c'est à dire une entrée de temps provenant du même employé, pour le même contrat, la même journée.
insert into f_workhours(fk_contract, fk_client, fk_date, fk_employee, fk_role, fk_contract_temp, fk_client_temp,
                        fk_date_temp, fk_employee_temp, fk_role_temp, Hours, Rate, Status, signedBy)
select null,
       null,
       null,
       null,
       null,
       contractNumber,
       concat(Client_CompanyName, ' ', Client_CompanyUnit)     as 'fk_client',
       workhours_date                                          as 'fk_date',
       concat(employee_firstname, ' ', employee_lastname) as 'fk_employee',
       employee_roleName                                       as 'fk_role',
       workhours_hours,
       workhours_rate,
       workhours_status,
       signedBy
from src_amcc_workhours
where concat(contractNumber, ' ', employee_firstname, ' ', employee_lastname, ' ', Client_CompanyName, ' ',
             Client_CompanyUnit, ' ', workhours_date) not in (
          select name
          from (select concat(contractNumber, ' ', employee_firstname, ' ', employee_lastname, ' ', Client_CompanyName,
                              ' ', Client_CompanyUnit, ' ', workhours_date) as 'name',
                       sum(workhours_hours),
                       count(1)
                from src_amcc_workhours
                group by concat(contractNumber, ' ', employee_firstname, ' ', employee_lastname, ' ',
                                Client_CompanyName, ' ', Client_CompanyUnit, ' ', workhours_date)
                having count(1) > 1) as sub
      );


# Insertion des entrée de temps doublons. C'est à dire une entrée de temps provenant du même employé, pour le même contrat, la même journée.
insert into f_workhours(fk_contract, fk_client, fk_date, fk_employee, fk_role, fk_contract_temp, fk_client_temp,
                        fk_date_temp, fk_employee_temp, fk_role_temp, Hours, Rate, Status)

select null,
       null,
       null,
       null,
       null,
       contractNumber,
       concat(Client_CompanyName, ' ', Client_CompanyUnit)     as 'fk_client',
       workhours_date                                          as 'fk_date',
       concat(employee_firstname, ' ', employee_lastname, ' ') as 'fk_employee',
       employee_roleName                                       as 'fk_role',
       sum(workhours_hours),
       workhours_rate,
       'special'
from src_amcc_workhours
group by contractNumber, null, null, null, null, null, concat(Client_CompanyName, ' ', Client_CompanyUnit),
         workhours_date, concat(employee_firstname, ' ', employee_lastname, ' '), employee_roleName, workhours_rate
having count(1) > 1;


/* ----------------------------------------------
*
* Vérification de l'insertion des données
*
* ---------------------------------------------- */

# a. Doublons
select concat(fk_contract_temp, ' ', fk_client_temp, ' ', fk_employee_temp, ' ', fk_date_temp) as 'name',
       count(1)
from f_workhours
group by concat(fk_contract_temp, ' ', fk_client_temp, ' ', fk_employee_temp, ' ', fk_date_temp)
having count(1) > 1;