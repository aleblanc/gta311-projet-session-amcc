use `gta311-leba2516-sb`;

# Ajout de la clé primaire des contracts dans la table f_workhours
select fk_contract,
       fk_contract_temp,
       dc.ContractID
from `gta311-leba2516-sb`.f_workhours
inner join d_contracts dc on f_workhours.fk_contract_temp = dc.ContractNumber;

update f_workhours
inner join d_contracts dc on f_workhours.fk_contract_temp = dc.ContractNumber
set f_workhours.fk_contract = dc.ContractID;

# Ajout de la clée primaire des dates dans la table f_workhours
select fk_date_temp, fk_date, dd.Date, dd.DateID
from f_workhours
inner join d_dates dd on f_workhours.fk_date_temp = dd.Date;

update f_workhours
inner join d_dates dd on f_workhours.fk_date_temp = dd.Date
set f_workhours.fk_date = dd.DateID;

# Ajout de la clée primaire des roles(jobs) dans la table f_workhours
select fk_role_temp, fk_role, f_workhours.Rate, dr.RoleName, dr.RoleID, dr.Rate
from f_workhours
inner join d_roles dr on f_workhours.fk_role_temp = dr.RoleName and f_workhours.Rate = dr.Rate;

update f_workhours
inner join d_roles dr on f_workhours.fk_role_temp = dr.RoleName and f_workhours.Rate = dr.Rate
set f_workhours.fk_role = dr.RoleID;

# Ajout de la clée primaire des client dans la table f_workhours
select fk_client_temp, fk_client, dc.CompanyName, dc.ClientID
from f_workhours
inner join d_clients dc on f_workhours.fk_client_temp = concat(dc.CompanyName, ' ', dc.CompanyUnit);

update f_workhours
inner join d_clients dc on f_workhours.fk_client_temp = concat(dc.CompanyName, ' ', dc.CompanyUnit)
set f_workhours.fk_client = dc.ClientID;

# Ajout de la clée primaire des employees dans la table f_workhours
select fk_employee_temp, fk_employee, concat(de.FirstName, ' ', de.LastName, ' ') as 'name', de.EmployeeID
from f_workhours
inner join d_employees de on f_workhours.fk_employee_temp = concat(de.FirstName, ' ', de.LastName);

update f_workhours
inner join d_employees de on f_workhours.fk_employee_temp = concat(de.FirstName, ' ', de.LastName)
set f_workhours.fk_employee = de.EmployeeID;