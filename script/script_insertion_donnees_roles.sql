use `gta311-leba2516-sb`;


/* ----------------------------------------------
*
* Validation et ajout des données de la table src_amcc_jobs
*
    RoleID   int auto_increment not null,
    RoleName varchar(45)        not null,
    `Rank`   varchar(45)        not null,
    Rate     decimal(16, 4)     not null,
*
* ---------------------------------------------- */

/* ----------------------------------------------
*
* 1. Vérification de la qualitée des données
*
* ---------------------------------------------- */
# a. Doublons
select title, count(1)
from src_amcc_jobs
group by title
having count(1) > 1;

/* ----------------------------------------------
*
* 2. Mise à jour des données
*
* ---------------------------------------------- */

# a. Poste double pour software engineer senior
update src_amcc_jobs
set level = 'junior'
where title = 'Software Engineer' and rate = 128.0000;


/* ----------------------------------------------
*
* Insertion des données
*
* ---------------------------------------------- */

insert into d_roles(RoleName, `Rank`, Rate)
    select title, level, rate
from src_amcc_jobs;

/* ----------------------------------------------
*
* Vérification de l'insertion des données
*
* ---------------------------------------------- */
