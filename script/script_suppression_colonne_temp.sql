use `gta311-leba2516-sb`;

alter table f_workhours drop column fk_employee_temp;
alter table f_workhours drop column fk_role_temp;
alter table f_workhours drop column fk_date_temp;
alter table f_workhours drop column fk_contract_temp;
alter table f_workhours drop column fk_client_temp;