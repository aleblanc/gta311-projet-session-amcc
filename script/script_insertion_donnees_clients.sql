use `gta311-leba2516-sb`;


/* ----------------------------------------------
*
* Validation et ajout des données de la table src_amcc_clients
*
#     ClientID     int auto_increment,img

#     CompanyName  varchar(40) not null,
#     CompanyUnit  varchar(40) not null,
#     ContactName  varchar(30) not null,
#     ContactTitle varchar(30) not null,
#     Address      varchar(60) not null,
#     City         varchar(15) not null,
#     Region       varchar(15) not null,
#     PostalCode   varchar(10) not null,
#     Country      varchar(15) not null,
#     Phone        varchar(24) not null,
#     Email        varchar(50) not null,
*
* ---------------------------------------------- */

/* ----------------------------------------------
*
* 1. Vérification de la qualitée des données
*
* ---------------------------------------------- */
# a. Doublons
select ContactName, count(1)
from src_amcc_clients
group by ContactName
having count(1) > 1;

# b. Données manquantes
select ContactName
from src_amcc_clients
where CompanyName is null
   or CompanyUnit is null
   or ContactName is null
   or ContactTitle is null
   or Address is null
   or City is null
   or Region is null
   or PostalCode is null
   or Country is null
   or Phone is null
   or Email is null;

# c. Qualitée de données pour le code postal (Format obligatoire A9A 9A9)
select ContactName,
       CompanyName,
       PostalCode
from src_amcc_clients
where Country = 'Canada'
  and PostalCode not REGEXP '^[A-Z][0-9][A-Z][ ][0-9][A-Z][0-9]$';

# d. Vérification du format pour la region
select Region, count(1)
from src_amcc_clients
group by Region;

# e. Vérification visuelle de présence de "Backslash \" dans les champs texte
select CompanyName,
       CompanyUnit,
       ContactName,
       ContactTitle,
       Address,
       City,
       Region,
       PostalCode,
       Country,
       Phone,
       Email
from src_amcc_clients
where CompanyName like '%\\\\%' or
       CompanyUnit like '%\\\\%' or
       ContactName like '%\\\\%' or
       ContactTitle like '%\\\\%' or
       Address like '%\\\\%' or
       City like '%\\\\%' or
       Region like '%\\\\%' or
       PostalCode like '%\\\\%' or
       Country like '%\\\\%' or
       Phone like '%\\\\%' or
       Email like '%\\\\%';

# f. Vérification du format courriel
select Email
from src_amcc_clients
where Email not REGEXP '^[A-Z0-9._%-]+@+[A-Z0-9.-]+\.[A-Z]{2,63}$';


/* ----------------------------------------------
*
* 2. Mise à jour des données
*
* ---------------------------------------------- */

# c. Ajout espace dans le format du code postal
update src_amcc_clients
set PostalCode = concat(substr(PostalCode, 1, 3), ' ', substr(PostalCode, 4))
where ContactName = 'Yves Malouin';

# d. Correction du libellé pour la région de Québec
update src_amcc_clients
set Region = 'QC'
where Region = 'Québec';

# e. Retrait des "Backslash \" dans les champs textes
update src_amcc_clients
set CompanyName = replace(CompanyName, '\\', ''),
    ContactName = replace(ContactName, '\\', ''),
    Address     = replace(Address, '\\', ''),
    Email       = replace(Email, '\\', '')
where CompanyName like '%\\\\%'
   or ContactName like '%\\\\%'
   or Address like '%\\\\%'
   or Email like '%\\\\%';

# f. Retrait des accents dans le champs courriel
update src_amcc_clients
set Email = replace(Email, 'é', 'e');
update src_amcc_clients
set Email = replace(Email, 'è', 'e');
update src_amcc_clients
set Email = replace(Email, 'ô', 'o');
update src_amcc_clients
set Email = replace(Email, 'ç', 'c');


/* ----------------------------------------------
*
* Insertion des données
*
* ---------------------------------------------- */

insert into d_clients (CompanyName, CompanyUnit, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Email)
    select distinct CompanyName,
                    CompanyUnit,
                    ContactName,
                    ContactTitle,
                    Address,
                    City,
                    Region,
                    PostalCode,
                    Country,
                    Phone,
                    Email
from src_amcc_clients;

/* ----------------------------------------------
*
* Vérification de l'insertion des données
*
* ---------------------------------------------- */

# a. Doublons
select ContactName, count(1)
from d_clients
group by ContactName
having count(1) > 1;

# c. Qualitée de données pour le code postal (Format obligatoire A9A 9A9)
select ContactName,
       CompanyName,
       PostalCode
from d_clients
where Country = 'Canada'
  and PostalCode not REGEXP '^[A-Z][0-9][A-Z][ ][0-9][A-Z][0-9]$';

# e. Vérification visuelle de présence de "Backslash \" dans les champs texte
select CompanyName,
       CompanyUnit,
       ContactName,
       ContactTitle,
       Address,
       City,
       Region,
       PostalCode,
       Country,
       Phone,
       Email
from d_clients
where CompanyName like '%\\\\%' or
       CompanyUnit like '%\\\\%' or
       ContactName like '%\\\\%' or
       ContactTitle like '%\\\\%' or
       Address like '%\\\\%' or
       City like '%\\\\%' or
       Region like '%\\\\%' or
       PostalCode like '%\\\\%' or
       Country like '%\\\\%' or
       Phone like '%\\\\%' or
       Email like '%\\\\%';

# f. Vérification du format courriel
select Email
from d_clients
where Email not REGEXP '^[A-Z0-9._%-]+@+[A-Z0-9.-]+\.[A-Z]{2,63}$';