use `gta311-leba2516-sb`;


/* ----------------------------------------------
*
* Validation et ajout des données de la table src_amcc_contracts
*
    ContractID     int auto_increment not null,
    ContractNumber varchar(45)        not null,
    Start_date     date               not null,
    End_date       date               not null,
    Amount         decimal(16, 4)     not null,
    SignedBy       int                not null,
*
* ---------------------------------------------- */
use `gta311-leba2516-sb`;
/* ----------------------------------------------
*
* 1. Vérification de la qualitée des données
*
* ---------------------------------------------- */
# a. Doublons
select contractNumber, count(1)
from src_amcc_contracts
group by contractNumber
having count(1) > 1;

# b. Données manquantes
select contractNumber
from src_amcc_contracts
where ContractNumber is null
   or Start_date is null
   or End_date is null
   or Amount is null
   or SignedBy is null;

# c. Vérification du champ signedBy
select signedBy
from src_amcc_contracts
where signedBy not in (0, 1);


/* ----------------------------------------------
*
* 2. Mise à jour des données
*
* ---------------------------------------------- */



/* ----------------------------------------------
*
* Insertion des données
*
* ---------------------------------------------- */

insert into d_contracts(contractnumber, start_date, end_date, amount, signedby)
    select distinct contractNumber, start_date, end_date, amount, signedBy
from src_amcc_contracts;

/* ----------------------------------------------
*
* Vérification de l'insertion des données
*
* ---------------------------------------------- */

# a. Doublons
select contractNumber, count(1)
from d_contracts
group by contractNumber
having count(1) > 1;

# c. Vérification du champ signedBy
select signedBy
from d_contracts
where signedBy not in (0, 1);