use `gta311-leba2516-sb`;


/* ----------------------------------------------
*
* Validation et ajout des données de la table src_amcc_companies
*
#     CompanyID   int auto_increment,
#     CompanyName varchar(40)                                           not null,
#     CEO         varchar(50)                                           not null,
#     Address     varchar(60)                                           not null,
#     City        varchar(60)                                           not null,
#     Region      varchar(15)                                           not null,
#     PostalCode  varchar(10)                                           not null,
#     Country     varchar(15)                                           not null,
#     Type        enum ('SMB', 'SME', 'Large enterprise', 'Government') not null,
#     Size        int                                                   not null,
#     Description varchar(255)                                          null,
#     Note        text                                                  null,
*
* ---------------------------------------------- */

/* ----------------------------------------------
*
* 1. Vérification de la qualitée des données
*
* ---------------------------------------------- */
# a. Doublons
select DISTINCT CompanyName,
                count(1)
from src_amcc_companies
group by CompanyName
having count(1) > 1;

# b. Données manquantes
select CompanyName
from src_amcc_companies
where CompanyName is null
   or CEO is null
   or Address is null
   or Region is null
   or PostalCode is null
   or Country is null
   or Size is null;

# c. Qualitée de données pour le code postal (Format obligatoire A9A 9A9)
select CompanyID,
       CompanyName,
       PostalCode
from src_amcc_companies
where Country = 'Canada'
  and PostalCode not REGEXP '^[A-Z][0-9][A-Z][ ][0-9][A-Z][0-9]$';

# d. Vérification du format pour la region
select Region, count(1)
from src_amcc_companies
group by Region;

/* ----------------------------------------------
*
* 2. Mise à jour des données
*
* ---------------------------------------------- */

# c. Ajout espace dans le format du code postal
update src_amcc_companies
set PostalCode = concat(substr(PostalCode, 1, 3), ' ', substr(PostalCode, 4))
where CompanyID = 423;

# d. Correction du libellé pour la région de Québec
update src_amcc_companies
set Region = 'QC'
where Region = 'Québec';

/* ----------------------------------------------
*
* Insertion des données
*
* Classification des entreprises
   ------------------
* SMB : 0 à 100 employés
* SME : 101 à 999 employés
* Large enterprise : 1000+ employés
*
* ---------------------------------------------- */

insert into d_companies (CompanyID,
                         CompanyName,
                         CEO,
                         Address,
                         City,
                         Region,
                         PostalCode,
                         Country,
                         Type,
                         Size,
                         Description,
                         Note)
select CompanyID,
       CompanyName,
       CEO,
       Address,
       City,
       Region,
       PostalCode,
       Country,
       case when size <= 100 then 1 when size > 100 and size < 1000 then 2 when size >= 1000 then 3 end,
       Size,
       Description,
       Note
from src_amcc_companies;

/* ----------------------------------------------
*
* Vérification de l'insertion des données
*
* ---------------------------------------------- */

select size, Type
from d_companies
where type = 'Large enterprise'
  and size < 1000;

select size, Type
from d_companies
where type = 'SME'
  and (size > 1000 or size < 100);

select size, Type
from d_companies
where type = 'SMB'
  and size > 100;