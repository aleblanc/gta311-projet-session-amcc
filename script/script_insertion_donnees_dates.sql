use `gta311-leba2516-sb`;


/* ----------------------------------------------
*
* Validation et ajout des données de la table src_amcc_dates
*
    DateID    int auto_increment not null,
    Date      date               not null,
    Year      int                not null,
    Month     int                not null,
    Day       int                not null,
    IsWeekend tinyint            not null,
*
* ---------------------------------------------- */

/* ----------------------------------------------
*
* 1. Vérification de la qualitée des données
*
* ---------------------------------------------- */
# a. Doublons
select date , count(1)
from src_amcc_dates
group by date
having count(1) > 1;

/* ----------------------------------------------
*
* 2. Mise à jour des données
*
* ---------------------------------------------- */



/* ----------------------------------------------
*
* Insertion des données
*
* ---------------------------------------------- */

insert into d_dates(date, year, month, day, isweekend)
    select date, YEAR(DATE), MONTH(DATE), DAY(DATE), if(dayofweek(date) in (1, 7), 1, 0)
from src_amcc_dates;

/* ----------------------------------------------
*
* Vérification de l'insertion des données
*
* ---------------------------------------------- */

# a. isweekend
select date, dayname(date), IsWeekend
from d_dates;