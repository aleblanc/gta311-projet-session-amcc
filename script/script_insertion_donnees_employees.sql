use `gta311-leba2516-sb`;


/* ----------------------------------------------
*
* Validation et ajout des données de la table src_amcc_employees
*
    EmployeeID int auto_increment not null,
    LastName   varchar(20)        not null,
    FirstName  varchar(10)        not null,
    BirthDate  datetime           not null,
    HireDate   datetime           not null,
    Address    varchar(60)        not null,
    City       varchar(15)        not null,
    Region     varchar(15)        not null,
    PostalCode varchar(10)        not null,
    Country    varchar(15)        not null,
    HomePhone  varchar(24)        not null,
    CellPhone  varchar(24)        not null,
    ReportsTo  int                null,
    Salary     decimal(16, 4)     not null,
*
* ---------------------------------------------- */

/* ----------------------------------------------
*
* 1. Vérification de la qualitée des données
*
* ---------------------------------------------- */
# a. Doublons
select concat(FirstName, ' ', LastName), count(1)
from src_amcc_employees
group by concat(FirstName, ' ', LastName)
having count(1) > 1;

# b. Données manquantes
select concat(FirstName, ' ', LastName)
from src_amcc_employees
where LastName is null
   or FirstName is null
   or BirthDate is null
   or HireDate is null
   or Address is null
   or City is null
   or Region is null
   or PostalCode is null
   or Country is null
   or HomePhone is null
   or CellPhone is null
   or Salary is null;

# c. Qualitée de données pour le code postal (Format obligatoire A9A 9A9)
select concat(FirstName, ' ', LastName),
       PostalCode
from src_amcc_employees
where Country = 'Canada'
  and PostalCode not REGEXP '^[A-Z][0-9][A-Z][ ][0-9][A-Z][0-9]$';

# d. Vérification du format pour la region
select Region, count(1)
from src_amcc_clients
group by Region;


/* ----------------------------------------------
*
* 2. Mise à jour des données
*
* ---------------------------------------------- */

# c. Ajout espace dans le format du code postal
update src_amcc_clients
set PostalCode = concat(substr(PostalCode, 1, 3), ' ', substr(PostalCode, 4))
where ContactName = 'Yves Malouin';

# d. Correction du libellé pour la région de Québec
update src_amcc_clients
set Region = 'QC'
where Region = 'Québec';

# e. Retrait des "Backslash \" dans les champs textes
update src_amcc_clients
set CompanyName = replace(CompanyName, '\\', ''),
    ContactName = replace(ContactName, '\\', ''),
    Address     = replace(Address, '\\', ''),
    Email       = replace(Email, '\\', '')
where CompanyName like '%\\\\%'
   or ContactName like '%\\\\%'
   or Address like '%\\\\%'
   or Email like '%\\\\%';

# f. Retrait des accents dans le champs courriel
update src_amcc_clients
set Email = replace(Email, 'é', 'e');
update src_amcc_clients
set Email = replace(Email, 'è', 'e');
update src_amcc_clients
set Email = replace(Email, 'ô', 'o');
update src_amcc_clients
set Email = replace(Email, 'ç', 'c');


/* ----------------------------------------------
*
* Insertion des données
*
* ---------------------------------------------- */

insert into d_employees(lastname, firstname, birthdate, hiredate, address, city, region, postalcode, country, homephone, cellphone, reportsto, salary)
    select distinct FirstName,
                    LastName,
                    BirthDate,
                    HireDate,
                    Address,
                    City,
                    Region,
                    PostalCode,
                    Country,
                    HomePhone,
                    CellPhone,
                    null,
                    Salary
from src_amcc_employees;

/* ----------------------------------------------
*
* Vérification de l'insertion des données
*
* ---------------------------------------------- */
