use `gta311-leba2516-sb`;

-- Prj-qry01 : Quelle est la période, en nombre de jours,
-- couverte par les activités de consultation d’AMCC dans la base de données de démonstration ?

select datediff(B, A) 'Nombre de jours couvert par la base de données de démontration'
from (
         select (select date
                 from `gta311-leba2516-sb`.`src_amcc_dates`
                 order by date ASC
                 LIMIT 1) as A,

                (select date
                 from `gta311-leba2516-sb`.`src_amcc_dates`
                 order by date DESC
                 LIMIT 1) as B
         from `gta311-leba2516-sb`.`src_amcc_dates`
         LIMIT 1
     ) as C;