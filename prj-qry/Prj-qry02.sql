use `gta311-leba2516-sb`;

-- Prj-qry02 : Qui est le client le plus payant d’AMCC ?

# Client le plus payant au total.
select dc.CompanyName,
       sum(Hours)                     as 'Nombre d\'heure travaillé',
       sum(Hours * Rate)              as 'Valeur total facturé',
       sum(Hours * rate) / sum(Hours) as 'Valeur facturé à l\'heure'
from f_workhours
         left join d_clients dc on dc.ClientID = f_workhours.fk_client
group by fk_client
order by sum(Hours * Rate) DESC
LIMIT 1;

# Client le plus payant à l'heure
select fk_client,
       dc.CompanyName,
       sum(Hours)                     as 'Nombre d\'heure travaillé',
       sum(Hours * Rate)              as 'Valeur total facturé',
       sum(Hours * rate) / sum(Hours) as 'Valeur facturé à l\'heure'
from f_workhours
         left join d_clients dc on dc.ClientID = f_workhours.fk_client
group by fk_client
order by sum(Hours * rate) / sum(Hours) DESC
LIMIT 1;

