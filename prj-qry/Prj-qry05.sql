use `gta311-leba2516-sb`;

-- Prj-qry05 : En moyenne, combien d’employés sont affectés à un contrat ?

select sum(`nombre employe`) / count(1) as 'Moyenne d\'employés par contrat'
from (
         select fk_contract, count(fk_employee) as 'nombre employe'
         from (
                  select distinct fk_contract, fk_employee
                  from `gta311-leba2516-sb`.f_workhours
              ) as A
         group by fk_contract
     ) as B;
