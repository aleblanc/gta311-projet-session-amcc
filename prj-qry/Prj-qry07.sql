use `gta311-leba2516-sb`;

-- Prj-qry07 : Combien d’heures ont été travaillées les fins de semaine (week-end) chez AMCC ?

select sum(Hours) as 'Heures fin de semaine'
from `gta311-leba2516-sb`.f_workhours
         left join d_dates dd on dd.DateID = f_workhours.fk_date
where IsWeekend = 1;