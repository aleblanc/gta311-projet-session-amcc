use `gta311-leba2516-sb`;

-- Prj-qry04 : Combien d’entreprises par type ont plus d’une division, incluant HQ, ayant fait affaires avec AMCC ?

select Type, count(1) as 'Nombre d\'entreprise de ce type ayant plus d\'une division'
from (select dc.CompanyName,
             type,
             count(dc.CompanyUnit)
      from `gta311-leba2516-sb`.d_clients dc
               left join d_companies company on dc.CompanyName = company.CompanyName
      group by CompanyName, Type
    having count(dc.CompanyUnit) > 1) as le
group by Type;