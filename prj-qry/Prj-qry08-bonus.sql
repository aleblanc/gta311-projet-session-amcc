use `gta311-leba2516-sb`;

-- Prj-qry08 : Remplir le champ Salary des employés en fonction de leur rôle.

# Hypothèse de 40 heures semaines sur 52 semaines
update d_employees
    inner join f_workhours fw on d_employees.EmployeeID = fw.fk_employee
    inner join d_roles dr on dr.RoleID = fw.fk_role
set d_employees.Salary = dr.rate * 40 * 52;