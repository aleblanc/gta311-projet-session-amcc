use `gta311-leba2516-sb`;

-- Prj-qry03 : Quel est l’employé d’AMCC ayant gagné le plus durant la période couverte par les données
--             selon ses revenus liés à un contrat ?

select fk_employee,
       sum(Hours * rate) as 'Revenu depuis tous les temps',
       concat(FirstName, ' ', LastName) as 'Nom de l\'employé'
from `gta311-leba2516-sb`.f_workhours
         left join d_employees de on de.EmployeeID = f_workhours.fk_employee
group by fk_employee
order by sum(Hours * rate) DESC
LIMIT 1;

