use `gta311-leba2516-sb`;

-- Prj-qry06 : Quels sont les rôles les plus sollicités chez AMCC en nombre d’heures ?

select fk_role,
       dr.RoleName,
       sum(Hours)
from `gta311-leba2516-sb`.f_workhours
left join d_roles dr on dr.RoleID = f_workhours.fk_role
group by fk_role
order by sum(Hours) DESC;