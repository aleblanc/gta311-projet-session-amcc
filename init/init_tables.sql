use `gta311-leba2516-sb`;
-- Création de la Table d_employees
create table d_employees
(
    EmployeeID int auto_increment not null,
    LastName   varchar(20)        not null,
    FirstName  varchar(15)        not null,
    BirthDate  datetime           not null,
    HireDate   datetime           not null,
    Address    varchar(60)        not null,
    City       varchar(15)        not null,
    Region     varchar(15)        not null,
    PostalCode varchar(10)        not null,
    Country    varchar(15)        not null,
    HomePhone  varchar(24)        not null,
    CellPhone  varchar(24)        not null,
    ReportsTo  int                null,
    Salary     decimal(16, 4)     not null,
    constraint d_employees_pk
        primary key (EmployeeID)
);

-- Création de la table f_workhours
create table f_workhours
(
    fk_contract_temp varchar(50)    null,
    fk_client_temp   varchar(50),
    fk_date_temp     varchar(50),
    fk_employee_temp varchar(50),
    fk_role_temp     varchar(50),
    fk_contract      int            null,
    fk_client        int            null,
    fk_date          int            null,
    fk_employee      int            null,
    fk_role          int            null,
    Hours            decimal(16, 2) not null,
    Rate             decimal(16, 4) not null,
    Status           varchar(45)    not null,
    signedBy
);

-- Création de la table d_roles
create table d_roles
(
    RoleID   int auto_increment not null,
    RoleName varchar(55)        not null,
    `Rank`   varchar(45)        not null,
    Rate     decimal(16, 4)     not null,
    constraint d_roles_pk
        primary key (RoleID)
);

-- Création de la table d_contracts
create table d_contracts
(
    ContractID     int auto_increment not null,
    ContractNumber varchar(45)        not null,
    Start_date     date               not null,
    End_date       date               not null,
    Amount         decimal(16, 4)     not null,
    SignedBy       int                not null,
    constraint d_contracts_pk
        primary key (ContractID)
);

-- Création de la table d_dates
create table d_dates
(
    DateID    int auto_increment not null,
    Date      date               not null,
    Year      int                not null,
    Month     int                not null,
    Day       int                not null,
    IsWeekend tinyint            not null,
    constraint d_dates_pk
        primary key (DateID)
);


-- Création de la table d_clients
create table d_clients
(
    ClientID     int auto_increment,
    CompanyName  varchar(80) not null,
    CompanyUnit  varchar(40) not null,
    ContactName  varchar(30) not null,
    ContactTitle varchar(30) not null,
    Address      varchar(60) not null,
    City         varchar(30) not null,
    Region       varchar(15) not null,
    PostalCode   varchar(10) not null,
    Country      varchar(15) not null,
    Phone        varchar(24) not null,
    Email        varchar(50) not null,
    constraint d_clients_pk
        primary key (ClientID)
);

-- Création de la table d_companies
create table d_companies
(
    CompanyID   int auto_increment,
    CompanyName varchar(80)                                           not null,
    CEO         varchar(50)                                           not null,
    Address     varchar(60)                                           not null,
    City        varchar(50)                                           not null,
    Region      varchar(15)                                           not null,
    PostalCode  varchar(10)                                           not null,
    Country     varchar(15)                                           not null,
    Type        enum ('SMB', 'SME', 'Large enterprise', 'Government') not null,
    Size        int                                                   not null,
    Description varchar(255)                                          null,
    Note        text                                                  null,
    constraint d_companies_pk
        primary key (CompanyID)
);

create unique index d_companies_fk_client_uindex
    on d_companies (CompanyName);

create unique index d_clients_fk_companies_uindex
    on d_clients (CompanyName);


