use `gta311-leba2516-sb`;

-- Relation de la table f_employees avec elle-même
alter table d_employees
    add constraint d_employees_d_employees_EmployeeID_fk
        foreign key (ReportsTo) references d_employees (EmployeeID)
on update cascade on delete restrict;

-- Foreign keys de la table f_workhours
alter table f_workhours
    add constraint d_employees_f_workhours_fk_employee_fk
        foreign key (fk_employee) references d_employees (EmployeeID)
            on update cascade on delete restrict,

    add constraint f_workhours_d_roles_fk_role_fk
        foreign key (fk_role) references d_roles (RoleID)
            on update cascade on delete restrict,

    add constraint f_workhours_d_contracts_fk_contract_fk
        foreign key (fk_contract) references d_contracts (ContractID)
            on update cascade on delete restrict,

    add constraint f_workhours_d_dates_fk_date_fk
        foreign key (fk_date) references d_dates (DateID)
            on update cascade on delete restrict,

    add constraint f_workhours_d_clients_fk_client_fk
        foreign key (fk_client) references d_clients (ClientID)
            on update cascade on delete restrict;


-- Relation Clients avec Companies
alter table d_companies
    add constraint d_companies_d_clients_fk
        foreign key (CompanyName) references d_clients (CompanyName)
            on update cascade;

